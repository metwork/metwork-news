import json
from pathlib import Path
from yaml import load, Loader

data_path = Path().parent / "data" / "news.yml"
export_path = Path().parent / "public" / "news.json"

data = load(data_path.open(), Loader=Loader)

export_path.write_text(json.dumps(data))
